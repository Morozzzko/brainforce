# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.db.models import Sum, Count
from django.contrib.auth.models import User
from datetime import datetime
from quiz.forms import AnswerForm
import time
from .models import Answer, Question, UserStat


def view_about(request):
    if request.user.is_authenticated():
        social = request.user.social_auth.get(provider='vk-oauth2')
        return render(request, 'quiz/about.html', {
            'first_name': social.extra_data['first_name'],
            'last_name': social.extra_data['last_name']
        })
    return render(request, 'quiz/about.html')


def view_management(request):
    if request.method == "POST":
        answer = Answer.objects.get(pk=request.POST['answer'])
        answer.correct = request.POST['is_right'].lower() == "true"

        if len(Answer.objects.filter(question=answer.question, correct=True)) < 20 and answer.correct:
            answer.bonus = 5

        answer.save()

        return redirect('/')

    try:
        answer = Answer.objects.filter(correct=None)[0]
        social = answer.user.social_auth.get(provider='vk-oauth2')
        return render(request, 'quiz/management/index.html', {
            'answer': answer,
            'first_name': social.extra_data['first_name'],
            'last_name': social.extra_data['last_name'],
            'uid': social.uid,
        })

    except (Answer.DoesNotExist, KeyError, IndexError):
        questions = Question.objects.all().order_by('date')

        question_data = []
        users = []

        for question in questions:
            answers = Answer.objects.filter(question=question, bonus__gt=0).order_by('pk')
            question_data.append((question, answers,))

        for user in User.objects.filter(is_staff=False, is_superuser=False):
            stat = UserStat(user)
            users.append(stat)

        users.sort(key=lambda x: x.get_total(), reverse=True)

        return render(request, 'quiz/management/stats.html', {
            'questions': question_data,
            'users': users,
        })


def view_index(request):
    brainiac_start_date = datetime.strptime('2016-04-20 18:00', '%Y-%m-%d %H:%M')
    now = datetime.fromtimestamp(time.time())
    time_delta = brainiac_start_date - now
    if request.user.is_staff:
        return view_management(request)
    try:
        question = Question.get_current_question()
        if not request.user.is_authenticated():
            return render(request, 'quiz/index_question_guest.html', {'question': question})

        social = request.user.social_auth.get(provider='vk-oauth2')

        try:
            answer = Answer.objects.get(question=question, user=request.user)

            return render(request, 'quiz/index_question_answered.html', {
                'question': question,
                'first_name': social.extra_data['first_name'],
                'last_name': social.extra_data['last_name'],
                'answer': answer.text,
            })

        except Answer.DoesNotExist:
            pass

        if request.method == "POST":
            form = AnswerForm(request.POST)
            if form.is_valid():
                answer = Answer(user=request.user, question=question, text=form.cleaned_data['text'])
                answer.save()
                return redirect('/')
        else:
            form = AnswerForm()

        return render(request, 'quiz/index_question.html', {
            'question': question,
            'first_name': social.extra_data['first_name'],
            'last_name': social.extra_data['last_name'],
            'form': form,
        })

    except Question.DoesNotExist:
        pass

    if time_delta.days > 1:
        return render(request, 'quiz/index_not_started.html', {'time_delta': time_delta})
    elif request.user.is_authenticated():

        stat = UserStat(request.user)

        return render(request, 'quiz/index_finished.html', {
            'question_points': stat.get_question_points(),
            'bonus_points': stat.get_bonus_points(),
            'count': stat.get_count(),
            'total_points': stat.get_total(),
        })
    return render(request, 'quiz/index_finished.html')
