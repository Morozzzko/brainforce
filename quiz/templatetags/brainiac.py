from django import template

register = template.Library()


def num_plural(number, pluralized_strings):
    args = [x.strip() for x in pluralized_strings.split(',')]
    assert len(args) == 3, "num_plural_ru expects 3 pluralized strings, {num} given".format(num=len(args))
    if number % 10 == 1 and number % 100 != 11:
        return args[0]
    if number % 10 >= 2 and number %10 <= 4 and (number % 100 < 10 or number % 100 >= 20):
        return args[1]
    return args[2]

register.filter('num_plural_ru', num_plural)