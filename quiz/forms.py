# -*- coding: utf-8 -*-

from django import forms
from quiz.models import Answer


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('text',)
