$(document).ready(function() {
	var boxWidth = 400;
	function centerBox() {
		var winWidth = $(window).width();
		var winHeight = $(document).height();
		var scrollPos = $(window).scrollTop();
		var disWidth = (winWidth - boxWidth) / 2
		var disHeight = scrollPos + 150;
		$('.popup-box').css({'width' : boxWidth+'px', 'left' : disWidth+'px', 'top' : disHeight+'px'});
		$('#blackout').css({'width' : winWidth+'px', 'height' : winHeight+'px'});
		return false;
	}
	$(window).resize(centerBox);
	$(window).scroll(centerBox);
	centerBox();
	$('[class*=popup-link]').click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		var name = $(this).attr('class');
		var id = name[name.length - 1];
		var scrollPos = $(window).scrollTop();
		$('#popup-box-'+id).show();
		$('#blackout').show();
		$('html').scrollTop(scrollPos);
	});
	$('[class*=popup-box]').click(function(e) { 
		e.stopPropagation(); 
	});
	$('html').click(function() { 
		$('[id^=popup-box-]').hide(); 
		$('#blackout').hide(); 
	});
});