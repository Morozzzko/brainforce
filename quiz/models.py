# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
import time


class UserStat:
    def __init__(self, usr):
        self.user = usr
        try:
            self.social = usr.social_auth.get(provider='vk-oauth2')
        except:
            pass
        self.stat = Answer.objects.filter(user=usr, correct=True).aggregate(
            question_points=models.Sum('question__value'),
            bonus_points=models.Sum('bonus'),
            count=models.Count('*'),
        )
        self.total = self.get_total()
        self.bonus = self.get_bonus_points()
        self.points = self.get_question_points()
        self.count = self.get_count()

    def get_total(self):
        try:
            return self.get_bonus_points() + self.get_question_points()
        except TypeError:
            return 0

    def get_count(self):
        return self.stat['count'] if self.stat['count'] is not None else 0

    def get_bonus_points(self):
        return self.stat['bonus_points'] if self.stat['bonus_points'] is not None else 0

    def get_question_points(self):
        return self.stat['question_points'] if self.stat['question_points'] is not None else 0


class Question(models.Model):
    text = models.TextField()
    suggestedAnswer = models.TextField()
    pic = models.ImageField(blank=True)
    value = models.PositiveSmallIntegerField()
    date = models.DateField(unique=True)

    def __str__(self):
        return "Question for {date}".format(date=self.date)

    @staticmethod
    def get_current_question():
        now = datetime.fromtimestamp(time.time())
        if now.hour < 18:
            return Question.objects.get(date=(now - timedelta(days=1)).date())

        return Question.objects.get(date=now.date())


class Answer(models.Model):
    user = models.ForeignKey(User)
    text = models.TextField()
    question = models.ForeignKey(Question)
    time = models.DateTimeField(auto_created=True, auto_now_add=True)
    correct = models.NullBooleanField(null=True)
    bonus = models.PositiveSmallIntegerField(null=True,
                                             default=0)

    def __str__(self):
        return "{user}'s answer ({question})".format(user=self.user, question=self.question)
