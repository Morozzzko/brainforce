from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^about/', views.view_about, name='about'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^$', views.view_index, name='index')
]
